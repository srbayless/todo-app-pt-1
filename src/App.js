import React, { Component } from "react";
import todosList from "./todos.json";
class App extends Component {
  state = {
    todos: todosList,
    addedtodo: "",
  };
  todoOutput = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  todoInput = (event) => {
    if (event.keyCode === 13) {
      const replaceTodos = this.state.todos;
      const newTodos = {
        userId: 1,
        id: Math.random(),
        title: this.state.addedtodo,
        completed: false,
      };
      replaceTodos.push(newTodos);
      this.setState({ todos: replaceTodos, addedtodo: "" });
    }
  };

  toggleTodos = (todoId) => (event) => {
    const newTodos = this.state.todos;
    let newNewTodos = newTodos.map((todo) => {
      if (todo.id === todoId) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    this.setState({ todos: newNewTodos });
  };
  removeX = (id) => {
    const newArr = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: newArr });
  };
  clearTodos = (i) => {
    const newArr = this.state.todos.filter((todo) => todo.completed === false);
    this.setState({ todos: newArr });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            name="addedtodo"
            value={this.state.addedtodo}
            onChange={this.todoOutput}
            onKeyDown={this.todoInput}
            placeholder="What needs to be done?"
            autofocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          toggleTodos={this.toggleTodos}
          removeX={this.removeX}
          clearTodos={this.clearTodos}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearTodos}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick={this.props.toggleTodos}
            onClick={this.props.clearTodos}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.removeX()} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              toggleTodos={this.props.toggleTodos(todo.id)}
              removeX={() => this.props.removeX(todo.id)}
              clearTodos={this.props.toggleTodos(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
